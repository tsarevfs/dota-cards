#!/usr/bin/python
from random import choice
from Cards.character_card import CharacterCard
from player import Player
from Cards.skill_card import SkillCard


class GameSystem(object):
   def __init__(self):
      self._main_window = None
      self._finished = False

   def finished(self):
      return self._finished

   def init(self, main_window):
      self._main_window = main_window

      sp = self._main_window.get_stripe_panel()
      esp = self._main_window.get_enemy_stripe_panel()
      
      #players initialisation
      self._player = Player(sp)
      self._enemy_player = Player(esp)

      character_cards = ["bounty_hunter", "zeus", "lina", "pudge"]
      for i in range(6):
         self._player.add(CharacterCard(self, sp.get_context(), choice(character_cards)))
         self._enemy_player.add(CharacterCard(self, esp.get_context(), choice(character_cards)))


      character_cards = ["meat_hook"]
      for i in range(3):
         self._player.add(SkillCard(self, sp.get_context(), choice(character_cards)))
         self._enemy_player.add(SkillCard(self, esp.get_context(), choice(character_cards)))


      self.start()

   def start(self):
      self._step = 0
      self._enemy_player_turn = False 
      self._print_stroke_info()
      self._player.set_active(not self._enemy_player_turn)
      self._enemy_player.set_active(self._enemy_player_turn)



   def end_of_stroke(self):
      if not self._enemy_player_turn:
         self._enemy_player_turn = True
      else:
         self._enemy_player_turn = False
         self._step += 1

      self._player.set_active(not self._enemy_player_turn)
      self._enemy_player.set_active(self._enemy_player_turn)

      self._print_stroke_info()


   def print_info(self, text):
      self._main_window.get_info_panel().set_text(text)

   def print_msg(self, text):
      self._main_window.get_msg_panel().set_text(text)

   def set_buttons(self, buttons_data):
      self._main_window.get_control_panel().set_buttons(buttons_data)

   def _print_stroke_info(self):
      self.print_msg("Step #%d, %s turn." % (self._step, "enemy" if self._enemy_player_turn else "your"))


if __name__ == "__main__":
   print "not executable"