#!/usr/bin/python
import json
import os
import numpy as np
import pygame

from context import Context

from GUI.mouse_control import MouseControlManager
from GUI.stripe_panel import StripePanel
from GUI.info_panel import InfoPanel
from GUI.msg_panel import MsgPanel
from GUI.control_panel import ControlPanel

class MainWindow(object):
   def __init__(self, game_system):
      self._game_system = game_system
      main_window_params = json.load(open(os.path.join("data", "classes.json")))["main_window"]
      self._window = pygame.display.set_mode(main_window_params["size"]) 
      self._ctx = Context(self._window)

      ######################################

      self._manager = MouseControlManager(np.array([0, 0]), None)

      ######################################
      stripe_panel_pos = np.array(main_window_params["stripe_panel_pos"])
      stripe_panel_manager = MouseControlManager(stripe_panel_pos, self._manager)
      self._stripe_panel = StripePanel(self._ctx, stripe_panel_pos, stripe_panel_manager)

      self._manager.register_dragable(self._stripe_panel)
      self._manager.register_panel(self._stripe_panel, stripe_panel_manager)

      ######################################
      enemy_stripe_panel_pos = np.array(main_window_params["enemy_stripe_panel_pos"])
      enemy_stripe_panel_manager = MouseControlManager(enemy_stripe_panel_pos, self._manager)
      self._enemy_stripe_panel = StripePanel(self._ctx, enemy_stripe_panel_pos, enemy_stripe_panel_manager)

      self._manager.register_dragable(self._enemy_stripe_panel)
      self._manager.register_panel(self._enemy_stripe_panel, enemy_stripe_panel_manager)

      ######################################
      info_panel_pos = np.array(main_window_params["info_panel_pos"])
      self._info_panel = InfoPanel(self._ctx, info_panel_pos)
      
      ######################################
      msg_panel_pos = np.array(main_window_params["msg_panel_pos"])
      self._msg_panel = MsgPanel(self._ctx, msg_panel_pos)

      ######################################
      control_panel_pos = np.array(main_window_params["control_panel_pos"])
      control_panel_manager = MouseControlManager(control_panel_pos, self._manager)
      self._control_panel = ControlPanel(self._ctx, control_panel_pos, control_panel_manager)
      self._manager.register_panel(self._control_panel, control_panel_manager)


   def draw(self):
      self._ctx.clear()
      self._stripe_panel.draw()
      self._enemy_stripe_panel.draw()
      self._info_panel.draw()
      self._msg_panel.draw()
      self._control_panel.draw()

   def get_stripe_panel(self):
      return self._stripe_panel

   def get_enemy_stripe_panel(self):
      return self._enemy_stripe_panel

   def get_info_panel(self):
      return self._info_panel

   def get_msg_panel(self):
      return self._msg_panel

   def get_control_panel(self):
      return self._control_panel

   def on_mouse_down(self, pos, button):
      self._manager.on_mouse_down(pos, button)

   def on_mouse_up(self, pos, button):
      self._manager.on_mouse_up(pos, button)

   def on_mouse_move(self, pos):
      self._manager.on_mouse_move(pos)


if __name__ == "__main__":
   print "not executable"