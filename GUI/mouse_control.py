import numpy as np

class MouseDragControl(object):
   def hit_test(self, pos):
      pass

   def on_drag(self, start, pos, finish=False):
      pass
  
class MouseSelectControl(object):
   def __init__(self):
      self._selected = False

   def hit_test(self, pos):
      pass

   def on_select(self, select):
      self._selected = select

   def selected(self):
      return self._selected

class MouseCheckControl(object):
   def __init__(self):
      self._checked = False

   def hit_test(self, pos):
      pass

   def on_check(self, force=None):
      if force != None:
         self._checked = force
      else:
         self._checked = not self._checked

   def checked(self):
      return self._checked

class MousePanelControl(object):
   def hit_test(self, pos):
      pass



class MouseButtonControl(object):
   def hit_test(self, pos):
      pass

   def on_mouse_down(self):
      pass

   def on_mouse_up(self):
      pass
   
class MouseControlManager(object):
   def __init__(self, offset, parent):
      self._dragable_items = set()
      self._selectable_items = set()
      self._checkable_items = set()
      self._buttons = set()
      self._panels = set()
      self._drag_state = [False, None, None]
      self._pressed_button = None
      self._coord_offset = offset
      self._parent = parent
      self._selection = None

      self._selection_enable = True
      self._checking_enable = True

   def enable_checking(self, enable):
      self._checking_enable = enable

   def enable_selection(self, enable):
      self._selection_enable = enable

   def register_dragable(self, item):
      self._dragable_items.add(item)

   def register_panel(self, item, manager):
      self._panels.add((item, manager))

   def register_selectable(self, item):
      self._selectable_items.add(item)

   def register_checkable(self, item):
      self._checkable_items.add(item)

   def register_button(self, item):
      self._buttons.add(item)

   def unregister_all_buttons(self):
      self._buttons = set()

   def unregister_all_selectable(self):
      self.unselect_all()
      self._selectable_items = set()

   def unregister_all_checkable(self):
      for item in self._checkable_items:
         item.on_check(False)
      self._checkable_items = set()

   def select(self, item):
      #TO-DO: looks like it smell, but works. select(None) call came always from panel ierarchy root
      if item != None:
         self.unselect_all()
      
      self._selection = item
      for _item in self._selectable_items:
         _item.on_select(_item == item)

      for panel in self._panels:
         panel[1].select(item)

   def unselect_all(self):
      if self._parent != None:
         self._parent.unselect_all()
      else:
         self.select(None)

   def get_selection(self):
      if self._selection != None:
         return self._selection

      for panel in self._panels:
         if panel[1]._selection != None:
            return panel[1]._selection

      return None

   def win2ctx(self, pos):
      return np.array(pos) - self._coord_offset

   def on_mouse_down(self, pos, button):
      if button == 3:
         for item in self._dragable_items:
            if item.hit_test(self.win2ctx(pos)):
               self._drag_state = [False, item, self.win2ctx(pos)]
               break
      if button == 1:
         for item in self._selectable_items:
            if self._selection_enable and item.hit_test(self.win2ctx(pos)):
               self.select(item)
               break

         for item in self._checkable_items:
            if self._checking_enable and item.hit_test(self.win2ctx(pos)):
               item.on_check()
               break

         for item in self._buttons:
            if item.hit_test(self.win2ctx(pos)):
               self._pressed_button = item
               item.on_mouse_down()
               break

      for item, panel in self._panels:
         if item.hit_test(self.win2ctx(pos)):
            panel.on_mouse_down(pos, button)
            break
            
   def on_mouse_up(self, pos, button):
      if button == 3:
         if self._drag_state[0]:
            self._drag_state[1].on_drag(self._drag_state[2], self.win2ctx(pos), True)
         self._drag_state = [False, None, None]

      if button == 1:
         if self._pressed_button != None:
            for item in self._buttons:
               if item.hit_test(self.win2ctx(pos)):
                  if item == self._pressed_button:
                     item.on_mouse_up()
                     break

      for item, panel in self._panels:
         if item.hit_test(self.win2ctx(pos)):
            panel.on_mouse_up(pos, button)
            break

   def on_mouse_move(self, pos):
      if self._drag_state[1] != None:
         self._drag_state[0] = True

      if self._drag_state[0]:
         self._drag_state[1].on_drag(self._drag_state[2], self.win2ctx(pos))

      for item, panel in self._panels:
         if item.hit_test(self.win2ctx(pos)):
            panel.on_mouse_move(pos)
            break


if __name__ == "__main__":
   print "not executable"