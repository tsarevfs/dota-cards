#!/usr/bin/python
import json
import os

import numpy as np
import pygame

from context import Context
from GUI.mouse_control import MouseDragControl


class StripePanel(MouseDragControl):
   def __init__(self, ctx, pos, manager):
      self._parent_ctx = ctx
      self._items = []
      type_params = json.load(open(os.path.join("data", "classes.json")))["stripe_panel"]
      self._pos = pos
      self._size = np.array(type_params["size"])
      self._distance = np.array(type_params["offset"])
      self._first_offset = np.array(type_params["first_offset"])
      self._bg_color = type_params["bg_color"]
      self._border_color = type_params["border_color"]

      self._offset = 0
      self._rect = pygame.Rect(self._pos[0], self._pos[1], self._size[0], self._size[1])
      self._drag_offset = None

      self._manager = manager
      self._manager.enable_checking(False)

      self._ctx = Context(pygame.Surface(self._size))


   def set_items(self, items):
      self._manager.unregister_all_selectable()
      self._manager.unregister_all_checkable()

      self._items = items
      for item in self._items:
         self._manager.register_selectable(item)
         self._manager.register_checkable(item)
      self.update()

   def update(self):
      for i, item in enumerate(self._items):
         cur_pos = self._first_offset + np.array([self._offset, 0.]) + self._distance * i
         item.set_pos(cur_pos)

   def draw(self):
      self._ctx.clear()
      for item in self._items:
         item.draw()
      self._ctx.rect(self._border_color, np.array([0, 0]), self._size, 1)
      self._parent_ctx.blit(self._ctx, self._pos)


   def hit_test(self, pos):
      return self._rect.collidepoint(pos)

   def on_drag(self, start, pos, finish=False):
      if self._drag_offset == None:
         self._drag_offset = self._pos[0] + self._offset - start[0]
      self._offset = pos[0] + self._drag_offset

      content_len = self._distance[0] * len(self._items)
      max_offset = 0
      min_offset = -content_len + self._size[0] - self._first_offset[0]
     
      if self._offset < min_offset:
         self._offset = min_offset;

      if self._offset > max_offset:
         self._offset = max_offset;
     
      if finish:
         self._drag_offset = None

      self.update()

   def get_context(self):
      return self._ctx

   def get_items(self):
      return self._items


if __name__ == "__main__":
   print "not executable"