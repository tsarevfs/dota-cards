#!/usr/bin/python

import pygame
import numpy as np
import json
import os

class InfoPanel(object):
   def __init__(self, ctx, pos):
      self._ctx = ctx
      self._pos = pos
      self._text = ""

      type_params = json.load(open(os.path.join("data", "classes.json")))["info_panel"]
      
      self._size = type_params["size"]
      
      self._bg_color = type_params["bg_color"]
      self._border_color = type_params["border_color"]

      font_size = type_params["font_size"]
      self._font = pygame.font.Font(None, font_size)
      self._font.set_bold(False)
      self._font_color = type_params["font_color"]
      self._text_pos = type_params["text_pos"]

      self._update()

   def set_text(self, text):
      self._text = text
      self._update()

   def _update(self):
      self._imgs = [self._font.render(line, 1, self._font_color) for line in self._text.split('\n')]

   def draw(self):
      for i, img in enumerate(self._imgs):
         pos = self._pos + self._text_pos + i * np.array([0, self._font.get_height()])
         self._ctx.image(img, pos)
      self._ctx.rect(self._border_color, self._pos, self._pos + self._size, 1)

      

if __name__ == "__main__":
   print "not executable"