#!/usr/bin/python

import json
import os

import pygame
import numpy as np

from GUI.mouse_control import MouseButtonControl


class Button(MouseButtonControl):
   def __init__(self, ctx, pos, size, text, callback, disabled=False):
      self._ctx = ctx
      self._pos = pos
      self._text = text
      self._size = size
      self._rect = pygame.Rect(self._pos[0], self._pos[1], self._size[0], self._size[1])
      self._text = text
      self._callback = callback
      self._disabled = disabled

      type_params = json.load(open(os.path.join("data", "classes.json")))["button"]
      self._bg_color = type_params["bg_color"]
      self._bg_disabled_color = type_params["bg_disabled_color"]
      self._border_color = type_params["border_color"]

      font_size = type_params["font_size"]
      font = pygame.font.Font(None, font_size)
      font.set_bold(False)
      font_color = type_params["font_color"]
      self._text_img = font.render(self._text, 1, font_color)
      self._text_pos = self._pos + self._size / 2 -\
         np.array([self._text_img.get_width(), self._text_img.get_height()]) / 2

   def draw(self):
      bg_color = self._bg_color if not self._disabled else self._bg_disabled_color
      self._ctx.rect(bg_color, self._pos, self._pos + self._size, 0)
      self._ctx.image(self._text_img, self._text_pos)
      self._ctx.rect(self._border_color, self._pos, self._pos + self._size, 1)

   def on_mouse_up(self):
      if not self._disabled:
         self._callback()

   def hit_test(self, pos):
      return self._rect.collidepoint(pos)


if __name__ == "__main__":
   print "not executable"