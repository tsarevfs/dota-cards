#!/usr/bin/python

import json
import os

import pygame
import numpy as np

from context import Context
from GUI.button import Button
from GUI.mouse_control import MousePanelControl


class ControlPanel(MousePanelControl):
   def __init__(self, ctx, pos, manager):
      self._parent_ctx = ctx
      self._pos = pos
      self._manager = manager

      type_params = json.load(open(os.path.join("data", "classes.json")))["control_panel"]
      
      self._size = type_params["size"]
      self._ctx = Context(pygame.Surface(self._size))
      self._rect = pygame.Rect(self._pos[0], self._pos[1], self._size[0], self._size[1])

      self._bg_color = type_params["bg_color"]
      self._border_color = type_params["border_color"]

      self._buttons = []

   def draw(self):
      self._ctx.rect(self._bg_color, np.array([0, 0]), self._size, 0)
      for button in self._buttons:
         button.draw()
      self._ctx.rect(self._border_color, np.array([0, 0]), self._size, 1)
      self._parent_ctx.blit(self._ctx, self._pos)

   def set_buttons(self, buttons_data):
      self._manager.unregister_all_buttons()
      for button_data in buttons_data:
         params = [self._ctx]
         params.extend(button_data)
         new_button = Button(*params)
         self._buttons.append(new_button)
         self._manager.register_button(new_button)

   def hit_test(self, pos):
      return self._rect.collidepoint(pos)


if __name__ == "__main__":
   print "not executable"