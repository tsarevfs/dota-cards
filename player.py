#!/usr/bin/python

class Player(object):
   def __init__(self, panel):
      self._panel = panel
      self._items = []

   def set_active(self, active):
      for item in self._items:
         item.set_active(active)

   def add(self, item):
      self._items.append(item)
      self._sort_items()
      self._panel.set_items(self._items)

   def _sort_items(self):
      pass

if __name__ == "__main__":
   print "not executable"