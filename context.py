import pygame

class Context(object):
   def __init__(self, surface=None):
      self._surface = surface
      self.width = surface.get_width()
      self.height = surface.get_height()
      self.bgColor = (0, 0, 0)

   def toScreen(self, point):
      return [int(point[0]), int(point[1])] 

   def line(self, color, p1, p2):
      pygame.draw.line(self._surface, color, self.toScreen(p1), self.toScreen(p2))

   def polyline(self, color, closed, points):
      pygame.draw.lines(self._surface, color, closed, [self.toScreen(p) for p in points])

   def circle(self, color, pos, radius, width=0):
      pygame.draw.circle(self._surface, color, self.toScreen(pos), radius, width)

   def rect(self, color, p1, p2, width=0):
      pts = [p1[0], p1[1], p2[0] - p1[0], p2[1] - p1[1]]
      pygame.draw.rect(self._surface, color, pts, width)

   def clear(self):
      self._surface.fill(self.bgColor)

   def image(self, img, pos):
      self._surface.blit(img, pos)

   def blit(self, other, pos):
      self._surface.blit(other._surface, pos)

if __name__ == "__main__":
   print "Not executable"
