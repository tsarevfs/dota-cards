#!/usr/bin/python
import json
import os
import numpy as np
import pygame

from Cards.base_card import BaseCard

class CharacterState(object):
   def __init__(self, params):
      self._hp = params["start_hp"]

class CharacterCard(BaseCard):


   def __init__(self, game_system, ctx, name):
      super(CharacterCard, self).__init__(game_system, ctx, name)

      params = json.load(open(os.path.join("data", "cards.json")))[name]
      type_params = json.load(open(os.path.join("data", "classes.json")))["character_card"]


      self._img_pos = np.array(type_params["img_pos"])
      img_path = type_params["img_path"][:]
      img_path.append(params["img"])
      img_name = os.path.join(*img_path)
      img = pygame.image.load(img_name).convert()
      self._img_size = np.array(type_params["img_size"])
      self._img = pygame.transform.scale(img, self._img_size)

      self._load_icons(type_params)

      self._name_pos = np.array(type_params["name_pos"]) - np.array([self._name_img.get_width() / 2, 0])

   def draw(self):
      super(CharacterCard, self).draw()

      self._ctx.image(self._img, self._pos + self._img_pos)
      self._ctx.rect(self._border_color, self._pos + self._img_pos, self._pos + self._img_pos + self._img_size, 1)

      self._ctx.image(self._name_img, self._pos + self._name_pos)

      self._ctx.image(self._icons["hp"][0], self._pos + self._icons["hp"][1])
      self._ctx.image(self._icons["attack"][0], self._pos + self._icons["attack"][1])
      self._ctx.image(self._icons["mp_regen"][0], self._pos + self._icons["mp_regen"][1])
      self._ctx.image(self._icons["hp_regen"][0], self._pos + self._icons["hp_regen"][1])


      if self._checked:
         self._ctx.image(self._icons["tick"][0], self._pos + self._icons["tick"][1]) #TO-DO move to base


if __name__ == "__main__":
   print "Not executable"