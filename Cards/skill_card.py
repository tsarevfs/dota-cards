#!/usr/bin/python
import json
import os
import numpy as np
import pygame

from Cards.base_card import BaseCard

class SkillCard(BaseCard):
   def __init__(self, game_system, ctx, name):
      super(SkillCard, self).__init__(game_system, ctx, name)

      params = json.load(open(os.path.join("data", "cards.json")))[name]
      type_params = json.load(open(os.path.join("data", "classes.json")))["skill_card"]

      self._owner = params["owner"]

      self._img_pos = np.array(type_params["img_pos"])
      img_path = type_params["img_path"][:]
      img_path.append(self._owner)
      img_path.append(params["img"])
      img_name = os.path.join(*img_path)
      img = pygame.image.load(img_name).convert()
      self._img_size = np.array(type_params["img_size"])
      self._img = pygame.transform.scale(img, self._img_size)

      self._load_icons(type_params)

      self._name_pos = np.array(type_params["name_pos"]) - np.array([self._name_img.get_width() / 2, 0])

   def draw(self):
      super(SkillCard, self).draw()

      self._ctx.image(self._img, self._pos + self._img_pos)
      self._ctx.rect(self._border_color, self._pos + self._img_pos, self._pos + self._img_pos + self._img_size, 1)

      self._ctx.image(self._name_img, self._pos + self._name_pos)

      self._ctx.image(self._icons["attack"][0], self._pos + self._icons["attack"][1])
      self._ctx.image(self._icons["manacost"][0], self._pos + self._icons["manacost"][1])


      if self._checked:
         self._ctx.image(self._icons["tick"][0], self._pos + self._icons["tick"][1]) #TO-DO move to base


if __name__ == "__main__":
   print "Not executable"