#!/usr/bin/python

from Cards.base_card import BaseCard

class ItemCard(BaseCard):
   def draw(self):
     super(ItemCard, self).draw()

   def __init__(self, game_system, ctx, name):
      super(ItemCard, self).__init__(game_system, ctx, name)

if __name__ == "__main__":
   print "Not executable"
