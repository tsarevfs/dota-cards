import json
import os

import numpy as np
import pygame

from GUI.mouse_control import MouseSelectControl, MouseCheckControl


class BaseCard(MouseSelectControl, MouseCheckControl):
   def __init__(self, game_system, ctx, name):
      MouseSelectControl.__init__(self)
      MouseCheckControl.__init__(self)

      self._game_system = game_system
      self._ctx = ctx
      self._pos = None

      self._name = name

      params = json.load(open(os.path.join("data", "cards.json")))[name]
      type_params = json.load(open(os.path.join("data", "classes.json")))["base_card"]

      self._size = np.array(type_params["size"])

      self._bg_color = type_params["bg_color"]
      self._border_color = type_params["border_color"]
      self._border_selected_color = type_params["border_selected_color"]

      font_size = type_params["font_size"]
      font = pygame.font.Font(None, font_size)
      font.set_bold(False)
      font_color = type_params["font_color"]

      self._icons = {}
      self._load_icons(type_params)

      self._printable_name = params["printable_name"]
      self._name_img = font.render(self._printable_name, 1, font_color)

      self._description = params["description"]

      self._rect = None
      self._active = False

   def draw(self):
      border_color = self._border_selected_color if self.selected() else self._border_color
      self._ctx.rect(border_color, self._pos, self._pos + self._size, 1)


      #if self._checked:
      #   self._ctx.image(self._icons["tick"][0], self._pos + self._icons["tick"][1])


   def set_pos(self, pos):
      self._pos = pos
      self._rect = pygame.Rect(self._pos[0], self._pos[1], self._size[0], self._size[1])

   def hit_test(self, pos):
      return self._rect.collidepoint(pos)

   def on_select(self, select):
      MouseSelectControl.on_select(self, select)

      if select:
       self._game_system.print_info(self._description)
       self._set_buttons()

   def _set_buttons(self):
      buttons_data = []

      hwf = lambda : self._game_system.print_msg("hello " + self._printable_name)
      buttons_data.append((np.array([10, 10]), np.array([50, 20]), "Hello", hwf))

      skip_f = self._game_system.end_of_stroke
      buttons_data.append((np.array([10, 40]), np.array([50, 20]), "Skip", skip_f, not self._active))

      self._game_system.set_buttons(buttons_data)

   def set_active(self, active):
      self._active = active
      if self._selected:
         self._set_buttons()

   def get_game_system(self):
      return self._game_system

   def _load_icons(self, type_params):
      icon_path = type_params["icon_path"]

      icons = type_params["icons"]
      for icon_desc in icons:
         img_path = icon_path[:]
         img_path.append(icon_desc["icon_img"])
         img_name = os.path.join(*img_path)
         img = pygame.image.load(img_name).convert_alpha()

         img_size = np.array(icon_desc["icon_size"])
         img_pos = np.array(icon_desc["icon_pos"])
         self._icons[icon_desc["icon_name"]] = (pygame.transform.scale(img, img_size), img_pos)


if __name__ == "__main__":
      print "Not executable"