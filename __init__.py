#!/usr/bin/python
import sys
import pygame

from GUI.main_window import MainWindow
from game_system import GameSystem

def main():
   pygame.init()

   game_system = GameSystem()
   main_window = MainWindow(game_system)
   game_system.init(main_window)

   dt = 10
   while not game_system.finished():
      main_window.draw()
      pygame.display.flip() 
      pygame.time.wait(dt)
      for event in pygame.event.get(): 
         if event.type == pygame.QUIT: 
             sys.exit(0) 
         elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            return
         elif event.type == pygame.MOUSEBUTTONDOWN:
            main_window.on_mouse_down(event.pos, event.button)
         elif event.type == pygame.MOUSEBUTTONUP:
            main_window.on_mouse_up(event.pos, event.button)
         elif event.type == pygame.MOUSEMOTION:
            main_window.on_mouse_move(event.pos)
   
if __name__ == "__main__":
   main()